class _BotCommands:
    def __init__(self):
        self.StartCommand = 'start2'
        self.MirrorCommand = 'mirror2'
        self.UnzipMirrorCommand = 'unzipmirror2'
        self.TarMirrorCommand = 'mirrortar2'
        self.CancelMirror = 'cancelmirror2'
        self.CancelAllCommand = 'cancelall2'
        self.ListCommand = 'list2'
        self.deleteCommand = 'del'
        self.StatusCommand = 'status2'
        self.AuthorizedUsersCommand = 'users'
        self.AuthorizeCommand = 'auth2'
        self.UnAuthorizeCommand = 'unauth2'
        self.AddSudoCommand = 'addsudo'
        self.RmSudoCommand = 'rmsudo'
        self.PingCommand = 'ping'
        self.RestartCommand = 'restart2'
        self.StatsCommand = 'stats2'
        self.HelpCommand = 'help2'
        self.LogCommand = 'logs2'
        self.CloneCommand = "clone2"
        self.WatchCommand = 'vid2'
        self.TarWatchCommand = 'vidtar2'
        self.RepoCommand = "repo2"
        self.UpdateCommand = "update2"

BotCommands = _BotCommands()
